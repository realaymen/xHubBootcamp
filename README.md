Leave Request Management Application

This is a web application built using Spring Boot and Java to manage leave requests for employees within a company. The application allows employees to submit leave requests, which are then deducted from their available leave balance if sufficient.
