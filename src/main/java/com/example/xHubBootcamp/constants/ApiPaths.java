package com.example.xHubBootcamp.constants;

public final class ApiPaths {
    public static final String V1 = "/api/v1";
    public static final String EMPLOYEE = "/employees";
    public static final String AUTH = "/auth";
    public static final String LEAVEREQUEST = "/leaverequests";
    public static final String LOGIN = "/login";
    public static final String REGISTER = "/register";

    public static final String VERIFY = "/verify";


}