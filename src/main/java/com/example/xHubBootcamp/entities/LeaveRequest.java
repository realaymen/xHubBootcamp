package com.example.xHubBootcamp.entities;

import com.example.xHubBootcamp.commands.LeaveRequestCommand;
import com.example.xHubBootcamp.enums.LeaveStatus;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@RequiredArgsConstructor
@Table(name = "leaveRequests")
public class LeaveRequest {
    @PrePersist
    protected void onCreate() {
        createdDate = LocalDateTime.now();
        updatedDate = LocalDateTime.now();
    }
    @PreUpdate
    protected void onUpdate() {
        updatedDate = LocalDateTime.now();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Long id;

    @JsonIgnore
    @ManyToOne
    private  Employee employee;

    private  LocalDate startDate;
    private  LocalDate endDate;
    private  Integer leaveDays;
    private  String reason;
    private LeaveStatus status = LeaveStatus.PENDING;
    private  String comments;
    private  LocalDateTime createdDate;
    private  LocalDateTime updatedDate;

    public  static LeaveRequest create (LeaveRequestCommand command) {
        LeaveRequest leaveRequest = new LeaveRequest();
        leaveRequest.setEmployee(command.getEmployee());
        leaveRequest.setStartDate(command.getStartDate());
        leaveRequest.setEndDate(command.getEndDate());
        leaveRequest.setLeaveDays(command.getLeaveDays());
        leaveRequest.setReason(command.getReason());
        leaveRequest.setStatus(command.getStatus());
        leaveRequest.setComments(command.getComments());
        leaveRequest.setCreatedDate(command.getCreatedDate());
        leaveRequest.setUpdatedDate(command.getUpdatedDate());
        return leaveRequest;
    }


}
