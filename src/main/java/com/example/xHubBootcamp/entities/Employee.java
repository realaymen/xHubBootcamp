package com.example.xHubBootcamp.entities;

import com.example.xHubBootcamp.commands.EmployeeCommand;
import com.example.xHubBootcamp.enums.Gender;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


@Getter
@Setter
@RequiredArgsConstructor
@Entity
@Table(name = "employees")

public class Employee {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private  Long id;
        private  String picture;
        private  String firstName;
        private  String lastName;
        @Column(unique = true)
        private  String email;
        private  Integer age;
        private  Gender gender;
        private  Date birthdayDate;
        private  String phone;
        private  String address;
        private  String bio;
        private  String job;
        private  BigDecimal salary;
        private  Integer leaveBalance = 30;
        @OneToMany(mappedBy = "employee", cascade = CascadeType.ALL)
        private  List<LeaveRequest> leaveRequests;

        public static Employee create(EmployeeCommand employeeCommand) {
                Employee employee = new Employee();
                employee.setPicture(employeeCommand.getPicture());
                employee.setFirstName(employeeCommand.getFirstName());
                employee.setLastName(employeeCommand.getLastName());
                employee.setEmail(employeeCommand.getEmail());
                employee.setAge(employeeCommand.getAge());
                employee.setGender(Gender.valueOf(employeeCommand.getGender().name()));
                employee.setBirthdayDate(employeeCommand.getBirthdayDate());
                employee.setPhone(employeeCommand.getPhone());
                employee.setAddress(employeeCommand.getAddress());
                employee.setBio(employeeCommand.getBio());
                employee.setJob(employeeCommand.getJob());
                employee.setSalary(employeeCommand.getSalary());

                return employee;
        }
        public void  update(EmployeeCommand employeeCommand) {

                this.setPicture(employeeCommand.getPicture());
                this.setFirstName(employeeCommand.getFirstName());
                this.setLastName(employeeCommand.getLastName());
                this.setEmail(employeeCommand.getEmail());
                this.setAge(employeeCommand.getAge());
                this.setGender(Gender.valueOf(employeeCommand.getGender().name()));
                this.setBirthdayDate(employeeCommand.getBirthdayDate());
                this.setPhone(employeeCommand.getPhone());
                this.setAddress(employeeCommand.getAddress());
                this.setBio(employeeCommand.getBio());
                this.setJob(employeeCommand.getJob());
                this.setSalary(employeeCommand.getSalary());

        }
}


