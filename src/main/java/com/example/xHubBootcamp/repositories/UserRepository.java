package com.example.xHubBootcamp.repositories;

import com.example.xHubBootcamp.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User,Integer> {
    User findByEmail(String email);
    User findByVerificationToken(String token);

}
