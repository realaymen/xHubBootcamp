package com.example.xHubBootcamp.exceptions;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.util.Date;

@RequiredArgsConstructor
@Getter
@Setter
public class ErrorMessage extends RuntimeException{
    private final String message;
    private final Date timestamp;
    private final HttpStatus httpStatus;
}
