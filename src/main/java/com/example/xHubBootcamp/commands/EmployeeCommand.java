package com.example.xHubBootcamp.commands;

import com.example.xHubBootcamp.enums.Gender;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@RequiredArgsConstructor
public class EmployeeCommand {


    private final String picture;

    @NotBlank
    private final String firstName;

    @NotBlank
    private final String lastName;

    @NotBlank
    @Email
    private final String email;

    @NotNull
    @Min(18)
    private final Integer age;

    @NotNull
    private final Gender gender;

    @NotNull
    private final Date birthdayDate;

    @NotBlank
    private final String phone;

    @NotBlank
    private final String address;

    @NotBlank
    private final String bio;

    @NotBlank
    private final String job;

    @NotNull
    private final BigDecimal salary;

}
