package com.example.xHubBootcamp.commands;


import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import com.example.xHubBootcamp.enums.Role;

import java.util.Set;


@Getter
@Setter
@RequiredArgsConstructor
public class RegisterCommand {
    @NotBlank
    private final String username;
    @NotBlank
    @Email
    private final String email;
    @NotBlank
    private final String password;
    @NotBlank
    private final Set<Role> roles;

}
