package com.example.xHubBootcamp.commands;

import com.example.xHubBootcamp.entities.Employee;
import com.example.xHubBootcamp.enums.LeaveStatus;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@RequiredArgsConstructor
public class LeaveRequestCommand {

    private final Employee employee;
    @NotNull
    private final LocalDate startDate;
    @NotNull
    private final LocalDate endDate;
    @NotNull
    private final int leaveDays;
    @NotBlank
    private final String reason;
    private LeaveStatus status = LeaveStatus.PENDING;
    @NotBlank
    private final String comments;
    private final LocalDateTime createdDate;
    private final LocalDateTime updatedDate;

}
