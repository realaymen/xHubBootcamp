package com.example.xHubBootcamp.commands;


import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class LoginCommand {
    @NotBlank
    @Email
    private final String email;
    @NotBlank
    private final String password;
}
