package com.example.xHubBootcamp.mappers;

import com.example.xHubBootcamp.dtos.EmployeeDTO;
import com.example.xHubBootcamp.entities.Employee;
import org.springframework.stereotype.Component;

@Component
public class EmployeeMapper {

    public EmployeeDTO toDTO(Employee employee) {
        if (employee == null)
            return null;
        EmployeeDTO employeeDTO = new EmployeeDTO();
        employeeDTO.setPicture(employee.getPicture());
        employeeDTO.setFirstName(employee.getFirstName());
        employeeDTO.setLastName(employee.getLastName());
        employeeDTO.setEmail(employee.getEmail());
        employeeDTO.setAge(employee.getAge());
        employeeDTO.setGender(employee.getGender());
        employeeDTO.setBirthdayDate(employee.getBirthdayDate());
        employeeDTO.setPhone(employee.getPhone());
        employeeDTO.setAddress(employee.getAddress());
        employeeDTO.setBio(employee.getBio());
        employeeDTO.setJob(employee.getJob());
        employeeDTO.setSalary(employee.getSalary());
        employeeDTO.setLeaveBalance(employee.getLeaveBalance());
        employeeDTO.setLeaveRequests(employee.getLeaveRequests());

        return employeeDTO;
    }

}
