package com.example.xHubBootcamp.mappers;

import com.example.xHubBootcamp.dtos.LeaveRequestDTO;
import com.example.xHubBootcamp.entities.LeaveRequest;
import org.springframework.stereotype.Component;

@Component
public class LeaveRequestMapper {
    public LeaveRequestDTO toDTO(LeaveRequest leaveRequest) {
        LeaveRequestDTO leaveRequestDTO = new LeaveRequestDTO();
        leaveRequestDTO.setStartDate(leaveRequest.getStartDate());
        leaveRequestDTO.setEndDate(leaveRequest.getEndDate());
        leaveRequestDTO.setLeaveDays(leaveRequest.getLeaveDays());
        leaveRequestDTO.setReason(leaveRequest.getReason());
        leaveRequestDTO.setStatus(leaveRequest.getStatus());
        leaveRequestDTO.setComments(leaveRequest.getComments());
        leaveRequestDTO.setCreatedDate(leaveRequest.getCreatedDate());
        leaveRequestDTO.setUpdatedDate(leaveRequest.getUpdatedDate());

        return leaveRequestDTO;
    }



}
