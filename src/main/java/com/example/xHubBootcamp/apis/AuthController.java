package com.example.xHubBootcamp.apis;


import com.example.xHubBootcamp.constants.ApiPaths;
import com.example.xHubBootcamp.commands.LoginCommand;
import com.example.xHubBootcamp.commands.RegisterCommand;
import com.example.xHubBootcamp.jwt.JwtServices;
import com.example.xHubBootcamp.services.user.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping(ApiPaths.V1 + ApiPaths.AUTH)
public class AuthController {
    private final AuthenticationManager authenticationManager;
    private final UserService userService;
    private final JwtServices jwtServices;

    @PostMapping(ApiPaths.REGISTER)
    public ResponseEntity<String> registerUser(@RequestBody RegisterCommand registerCommand) {
        userService.registerUser(registerCommand);
        return ResponseEntity.ok("User registered successfully. Check your email for verification.");
    }

    @GetMapping(ApiPaths.VERIFY)
    public ResponseEntity<String> verifyEmail(@RequestParam("token") String token) {
        userService.verifyUser(token);
        return ResponseEntity.ok("Email verified successfully.");
    }

    @PostMapping(ApiPaths.LOGIN)
    public ResponseEntity<String> authenticateUser(@RequestBody LoginCommand loginCommand) {
        try {
            log.info("Received login request for email: {}", loginCommand.getEmail());

            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            loginCommand.getEmail(),
                            loginCommand.getPassword()
                    )
            );

            SecurityContextHolder.getContext().setAuthentication(authentication);
            String token = jwtServices.generateToken(loginCommand.getEmail());
            log.info("Authentication successful for email: {}", loginCommand.getEmail());


            return new ResponseEntity<>(token, HttpStatus.OK);

        } catch (AuthenticationException e) {
            log.warn("Authentication failed for email: {}", loginCommand.getEmail());
            return new ResponseEntity<>("Invalid credentials.", HttpStatus.UNAUTHORIZED);
        }
    }
}