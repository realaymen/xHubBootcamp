package com.example.xHubBootcamp.apis;


import com.example.xHubBootcamp.commands.LeaveRequestCommand;
import com.example.xHubBootcamp.constants.ApiPaths;
import com.example.xHubBootcamp.dtos.LeaveRequestDTO;
import com.example.xHubBootcamp.mappers.LeaveRequestMapper;
import com.example.xHubBootcamp.services.LeaveRquest.LeaveRequestService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(ApiPaths.V1 + ApiPaths.EMPLOYEE + ApiPaths.LEAVEREQUEST)
@RequiredArgsConstructor
public class LeaveRequestController {

    private final LeaveRequestService leaveRequestService;
    private final LeaveRequestMapper leaveRequestMapper;


    @PostMapping("/{employeeId}")
    public ResponseEntity<LeaveRequestDTO> addLeaveRequest(
            @PathVariable Long employeeId,
            @RequestBody LeaveRequestCommand leaveRequestCommand
    ) {
        return ResponseEntity.ok(leaveRequestMapper.toDTO(leaveRequestService.addLeaveRequest(employeeId,leaveRequestCommand)));
    }

}


