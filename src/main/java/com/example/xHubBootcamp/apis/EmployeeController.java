package com.example.xHubBootcamp.apis;


import com.example.xHubBootcamp.commands.EmployeeCommand;
import com.example.xHubBootcamp.constants.ApiPaths;
import com.example.xHubBootcamp.dtos.EmployeeDTO;
import com.example.xHubBootcamp.mappers.EmployeeMapper;
import com.example.xHubBootcamp.services.employee.EmployeeService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;


@RestController
@RequestMapping(ApiPaths.V1 + ApiPaths.EMPLOYEE)
@RequiredArgsConstructor

public class EmployeeController {

    private final EmployeeService employeeService;
    private final EmployeeMapper employeeMapper;


    @GetMapping
    public ResponseEntity<Page<EmployeeDTO>> getAllEmployees(Pageable pageable) {
      return ResponseEntity.ok((employeeService.getAllEmployees(pageable).map(employeeMapper :: toDTO)));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Optional<EmployeeDTO>> getEmployeeById(@PathVariable Long id) {
        return ResponseEntity.ok((employeeService.getEmployeeById(id).map(employeeMapper :: toDTO)));
    }

    @PostMapping
    public ResponseEntity<EmployeeDTO> addEmployee(@Valid @RequestBody EmployeeCommand employeeCommand) {
        return ResponseEntity.ok(employeeMapper.toDTO(employeeService.addEmployee(employeeCommand)));
    }


    @PutMapping("/{id}")
    public ResponseEntity<EmployeeDTO> updateEmployee(
            @PathVariable Long id,
            @Valid @RequestBody EmployeeCommand employeeCommand) {

        return ResponseEntity.ok(employeeMapper.toDTO(employeeService.updateEmployee(id,employeeCommand)));
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteEmployee(@PathVariable Long id) {
        employeeService.deleteEmployee(id);
        return ResponseEntity.noContent().build();
    }




}
