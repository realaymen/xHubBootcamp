package com.example.xHubBootcamp.dtos;

import com.example.xHubBootcamp.enums.LeaveStatus;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;


import java.time.LocalDate;
import java.time.LocalDateTime;


@Getter
@Setter
@RequiredArgsConstructor
public class LeaveRequestDTO {

    private  LocalDate startDate;
    private  LocalDate endDate;
    private  Integer leaveDays;
    private  String reason;
    private  LeaveStatus status;
    private  String comments;
    private  LocalDateTime createdDate;
    private  LocalDateTime updatedDate;
}
