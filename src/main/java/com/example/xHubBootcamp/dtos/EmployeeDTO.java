package com.example.xHubBootcamp.dtos;

import com.example.xHubBootcamp.entities.LeaveRequest;
import com.example.xHubBootcamp.enums.Gender;
import lombok.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@RequiredArgsConstructor
public class EmployeeDTO {
    private  String picture;
    private  String firstName;
    private  String lastName;
    private  String email;
    private  Integer age;
    private  Gender gender;
    private  Date birthdayDate;
    private  String phone;
    private  String address;
    private  String bio;
    private  String job;
    private  BigDecimal salary;
    private  Integer leaveBalance;
    private  List<LeaveRequest> leaveRequests;


}
