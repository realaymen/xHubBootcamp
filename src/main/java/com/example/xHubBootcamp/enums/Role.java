package com.example.xHubBootcamp.enums;

public enum Role {
    ROLE_USER,
    ROLE_HR,
    ROLE_MANAGER,
    ROLE_ADMIN
}