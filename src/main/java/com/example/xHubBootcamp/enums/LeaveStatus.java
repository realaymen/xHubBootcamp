package com.example.xHubBootcamp.enums;

public enum LeaveStatus {
        PENDING,
        APPROVED,
        REJECTED,
        CANCELLED

}
