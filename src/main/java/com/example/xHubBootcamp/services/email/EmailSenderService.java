package com.example.xHubBootcamp.services.email;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
@Slf4j
public class EmailSenderService {

    public final JavaMailSender javaMailSender;

public void sendVerificationEmail(String toEmail,
                                  String subject,
                                  String body){
    SimpleMailMessage message = new SimpleMailMessage();
    message.setFrom("aymentrading7@gmail.com");
    message.setTo(toEmail);
    message.setText(body);
    message.setSubject(subject);

    javaMailSender.send(message);

    log.info("Mail sent Successfully ");

}




}
