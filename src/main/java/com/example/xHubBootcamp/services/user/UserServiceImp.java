package com.example.xHubBootcamp.services.user;

import com.example.xHubBootcamp.commands.RegisterCommand;
import com.example.xHubBootcamp.entities.User;
import com.example.xHubBootcamp.repositories.UserRepository;
import com.example.xHubBootcamp.services.email.EmailSenderService;
import lombok.RequiredArgsConstructor;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserServiceImp implements UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final EmailSenderService emailSenderService;


    public User registerUser(RegisterCommand registerCommand) {
        User user = new User();
        String token = UUID.randomUUID().toString();
        user.setVerificationToken(token);

        user.setUsername(registerCommand.getUsername());
        user.setEmail(registerCommand.getEmail());
        user.setRoles(registerCommand.getRoles());
        user.setPassword(passwordEncoder.encode(registerCommand.getPassword()));
        userRepository.save(user);

        emailSenderService.sendVerificationEmail(user.getEmail(), "Account Verification", user.getVerificationToken());
        return user;
    }

    public void verifyUser(String token) {
        User user = userRepository.findByVerificationToken(token);
        if (user != null) {
            user.setEmailVerified(true);
            userRepository.save(user);
        }
    }


}
