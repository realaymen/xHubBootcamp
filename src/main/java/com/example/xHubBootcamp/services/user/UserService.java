package com.example.xHubBootcamp.services.user;


import com.example.xHubBootcamp.commands.RegisterCommand;
import com.example.xHubBootcamp.entities.User;
import org.springframework.stereotype.Service;

@Service
public interface UserService {
    User registerUser(RegisterCommand registerCommand);
    void verifyUser(String token);
}
