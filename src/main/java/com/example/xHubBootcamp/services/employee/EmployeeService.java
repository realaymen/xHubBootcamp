package com.example.xHubBootcamp.services.employee;

import com.example.xHubBootcamp.commands.EmployeeCommand;
import com.example.xHubBootcamp.entities.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public interface EmployeeService {

    Page<Employee> getAllEmployees(Pageable pageable);

    Optional<Employee> getEmployeeById(Long id);

    Employee updateEmployee(Long id, EmployeeCommand employeeCommand);

    void deleteEmployee(Long id);

    Employee addEmployee(EmployeeCommand employeecommand);
}
