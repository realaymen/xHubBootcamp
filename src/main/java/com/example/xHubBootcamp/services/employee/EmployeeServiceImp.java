package com.example.xHubBootcamp.services.employee;

import com.example.xHubBootcamp.commands.EmployeeCommand;
import com.example.xHubBootcamp.entities.Employee;
import com.example.xHubBootcamp.exceptions.ApiRequestException;
import com.example.xHubBootcamp.repositories.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Service
public class EmployeeServiceImp implements EmployeeService {

    private final EmployeeRepository employeeRepository;

  @Override
    public Page<Employee> getAllEmployees(Pageable pageable) {
      return employeeRepository.findAll(pageable);
  }

    @Override
    public Optional<Employee> getEmployeeById(Long id) {
        return Optional.ofNullable(employeeRepository.findById(id))
                .orElseThrow(() -> new ApiRequestException("Employee not found with id: " + id));
    }

    @Override
    public Employee addEmployee(EmployeeCommand employeeCommand) {
        log.info("Start adding employee");
        if (employeeRepository.existsByEmail(employeeCommand.getEmail())) {
            throw new ApiRequestException("Employee with the same email already exists");
        }
        return employeeRepository.save(Employee.create(employeeCommand));
    }

    @Override
    public Employee updateEmployee(Long id, EmployeeCommand employeeCommand) {
        Employee existingEmployee = employeeRepository.findById(id)
                .orElseThrow(() -> new ApiRequestException("Employee not found with id: " + id));
        existingEmployee.update(employeeCommand);
        return employeeRepository.save(existingEmployee);
    }

    @Override
    public void deleteEmployee(Long id) {
        Employee employee = employeeRepository.findById(id)
                .orElseThrow(() -> new ApiRequestException("Employee not found with customerId: " + id));
        employeeRepository.delete(employee);
    }

}


