package com.example.xHubBootcamp.services.LeaveRquest;

import com.example.xHubBootcamp.commands.LeaveRequestCommand;
import com.example.xHubBootcamp.entities.LeaveRequest;
import org.springframework.stereotype.Service;


@Service
public interface LeaveRequestService {
    LeaveRequest addLeaveRequest(Long employeeId, LeaveRequestCommand leaveRequestCommand);

}
