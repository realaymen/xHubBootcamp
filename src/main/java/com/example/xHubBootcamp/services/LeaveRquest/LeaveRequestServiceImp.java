package com.example.xHubBootcamp.services.LeaveRquest;

import com.example.xHubBootcamp.commands.LeaveRequestCommand;
import com.example.xHubBootcamp.entities.Employee;
import com.example.xHubBootcamp.exceptions.ApiRequestException;
import com.example.xHubBootcamp.repositories.EmployeeRepository;
import com.example.xHubBootcamp.entities.LeaveRequest;
import com.example.xHubBootcamp.repositories.LeaveRequestRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class LeaveRequestServiceImp implements LeaveRequestService {
    private final EmployeeRepository employeeRepository;
    private final LeaveRequestRepository leaveRequestRepository;

    @Override
    public LeaveRequest addLeaveRequest(Long employeeId, LeaveRequestCommand leaveRequestCommand) {
        Employee employee = employeeRepository.findById(employeeId)
                .orElseThrow(() -> new ApiRequestException("Employee not found with id: " + employeeId));

        int requestedLeaveDays = leaveRequestCommand.getLeaveDays();
        int currentLeaveBalance = employee.getLeaveBalance();

        if (currentLeaveBalance < requestedLeaveDays)
            throw new ApiRequestException("Insufficient leave balance for employee with id: " + employeeId);

        employee.setLeaveBalance(currentLeaveBalance - requestedLeaveDays);

        LeaveRequest leaveRequest = LeaveRequest.create(leaveRequestCommand);
        leaveRequest.setEmployee(employee);
        return leaveRequestRepository.save(leaveRequest);
    }




}
