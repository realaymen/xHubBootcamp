package com.example.xHubBootcamp.jwt;

import com.example.xHubBootcamp.constants.SecurityConstant;

import com.example.xHubBootcamp.services.user.UserDetailServiceImp;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

@Component
@Slf4j
@RequiredArgsConstructor
public class JwtAuthFilter extends OncePerRequestFilter {

    private final UserDetailServiceImp userDetailServiceImp;
    private final JwtServices jwtServices;

    @Override
    protected void doFilterInternal(
           @NotNull HttpServletRequest request,
           @NotNull   HttpServletResponse response,
           @NotNull  FilterChain filterChain) throws ServletException, IOException {

        try {
            String token = getTokenFromRequest(request);
            log.debug("Received token from request: {}", token);

            if (token != null && jwtServices.validateToken(token)) {
                String username = jwtServices.extractUsername(token);
                log.debug("Authenticated user: {}", username);
                UserDetails userDetails = userDetailServiceImp.loadUserByUsername(username);

                UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
                        userDetails, null, userDetails.getAuthorities()
                );
                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            } else {
                log.debug("Authentication failed for token: {}", token);
            }
        } catch (Exception ex) {
            log.error("An error occurred during token authentication.", ex);
        }

        filterChain.doFilter(request, response);
    }

    private String getTokenFromRequest(HttpServletRequest request) {
        String bearerToken = request.getHeader("Authorization");
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith(SecurityConstant.TOKEN_PREFIX)) {
            String token = bearerToken.substring(SecurityConstant.TOKEN_PREFIX.length());
            log.debug("Received token from request: {}", token);
            return token;
        }
        return null;
    }

}
