package com.example.xHubBootcamp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XHubBootcampApplication {
    public static void main(String[] args) {
        SpringApplication.run(XHubBootcampApplication.class, args);
    }

}
